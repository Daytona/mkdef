# mkdef

A command line tool for making ClamAV hex based definitions

### Pros

* Custom clamav definitions would allow us to detect more malware and specifically malware that is targeted at our customers.
* We could eliminate some or all of the string/regex matching of shellscan to improve performance and reduce server load.
* Less reliance on third party vendors like Sucuri. Future possibilities of offering site cleaning services

### Cons

The only real problem with using this script is going to be changing the process of hacked accounts. Someone would have to to investigate further than running shellscan.

I submitted a heuristics scanner which would help in detecting undetected malware, but I guess T3 doesn't develop shellscan so it's currently unused.
https://trac.imhtech.net/T3/ticket/17267

### Prerequisites

Python 2.7 (will probably also run with 2.6)
All modules are standard

## Getting Started

1. Install mkdef.py to /usr/bin/mkdef on a definitions server
2. Configure the path for definitions to be saved on line 12: self.ndb_dir
3. (Optional) Make linux users for each person making definitions (useradd)
4. (Optional) Adjust permissions on this directory to allow these users to write to it. (or configure permissions as you see fit)
5. (Optional) Run mkdef as those users as that will be what the definitions database file will be named after. This allows to track who makes false positives or bad definitions.

## Usage

1. Find some text based malware not detected by clamav
2. Open the file and copy a unique indicator of this malware which will not cause false positives (payload is best)
3. Go to the definitions server and do the following:
4. su [YOUR_USER]
5. mkdef [DEFINITION_NAME]
6. Paste the string you got from step 2
7. CTRL + D to confirm

## Example of usage

The following example is actually set up on vps29516, you can connect and try it. If you run it as root it will write to root.ndb, feel free to useradd a new user if you like.

```
root@vps29516 [~]# su MathewL
MathewL@vps29516 [/root]# mkdef Cheetah.Shell
         __      __    ___
  __ _  / /_____/ /__ / _/
 /  ' \/  '_/ _  / -_) _/
/_/_/_/_/\_\_,_/\__/_/
Paste the text to make a definition for
ctrl + D to confirm
ctrl + C to quit
code("PD9waHANCiRjb2xvciA9ICIjYTNlOTU2

writing to /home/defs/public_html/MathewL.ndb:
MathewL.Cheetah.Shell.Norm:0:*:636f64652822706439776168616e6369726a62327876636961396963696a79746e6c6f747532

writing to /home/defs/public_html/MathewL.ndb:
MathewL.Cheetah.Shell:0:*:636f64652822504439776148414e4369526a62327876636941394943496a59544e6c4f545532

MathewL@vps29516 [/root]# exit

root@vps29516 [~]# ls -la /home/defs/public_html/
ls /home/defs/public_html/
./  ../  error_log  .htaccess  IMH-Literals.ndb  MathewL.ndb  .well-known/

```

## pylint

pylint rating 9.67/10

```
pylint mkdef.py
No config file found, using default configuration
************* Module mkdef
W: 91, 0: Anomalous backslash in string: '\/'. String constant might be missing an r prefix. (anomalous-backslash-in-string)
W: 91, 0: Anomalous backslash in string: '\_'. String constant might be missing an r prefix. (anomalous-backslash-in-string)
W: 91, 0: Anomalous backslash in string: '\_'. String constant might be missing an r prefix. (anomalous-backslash-in-string)

------------------------------------------------------------------
Your code has been rated at 9.67/10 (previous run: 9.67/10, +0.00)
```

## Deployment

The current setup on my VPS works, but I'd we may want to look into a better deployment strategy. Running the following bash script/command on a remote server will download and install the virus definitions to your current server.

```
server='https://defs.cpcmr.com/'
def_dbs='MathewL.ndb
IMH-Literals.ndb'
echo "$def_dbs" | while read x;
do
    echo "saving to /var/clamav/$x"
    curl -so "/var/clamav/$x" "$server$x"
done
```

## Authors

* **Mathew Long** -  [APS]
