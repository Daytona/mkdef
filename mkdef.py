#!/usr/bin/python
'''Module/Script for creating ClamAV definitions'''
# Written by Mathew Long
import re
import sys
import os
import getpass
from HTMLParser import HTMLParser

class MkDef(object):
    '''Main class for definition maker'''
    def __init__(self):
        '''sets user's database file name based on executing user'''
        self.ndb_dir = '/home/defs/public_html/'
        self.prefix = getpass.getuser()
        self.ndb_file = '{0}{1}.ndb'.format(self.ndb_dir, self.prefix)
        return

    def check_ndb(self):
        '''Makes NDB file if it doesnt exist'''
        open(self.ndb_file, 'a').close()
        return

    def check_name(self):
        '''Minor error checking on definition name'''
        if len(sys.argv) > 1:
            def_name = sys.argv[1]
        else:
            print 'Usage: mkdef [DEFINITION_NAME]'
            sys.exit(0)
        if re.match(r'[^0-9a-zA-Z\._\-]', def_name):
            print 'Error: Classifications may only \
                consist of alpha-numeric, dash, period, and underscore.'
            sys.exit(0)

        with open(self.ndb_file, 'r') as file_handle:
            db_file = file_handle.read()
        if def_name not in db_file:
            return def_name
        else:
            i = 1
            while '{0}.{1}'.format(def_name, str(i)) in db_file:
                i += 1
        return '{0}.{1}'.format(def_name, str(i))

    def definition_exists(self, definition):
        '''Checks if the definition already exists in any of the dbs'''
        files = [f for f in os.listdir(self.ndb_dir) \
            if os.path.isfile(os.path.join(self.ndb_dir, f))]
        for file_name in files:
            file_name = '{0}{1}'.format(self.ndb_dir, file_name)
            if not file_name.endswith('.ndb'):
                continue
            with open(file_name, 'r') as file_handle:
                if re.search(':{0}(\n|$)'.format(definition), file_handle.read()):
                    print 'Definition already in database {0}, skipping'.format(file_name)
                    return 1
        return

    def make_definition(self, def_name, definition):
        '''The function that actually writes the definition'''
        if self.definition_exists(definition):
            return
        with open(self.ndb_file, 'r') as file_handle:
            if re.search(':{0}\n'.format(definition), file_handle.read()):
                print 'Definition already in database, skipping'
                return
        full_def = '{0}.{1}:0:*:{2}\n'.format(
            self.prefix,
            def_name,
            definition)
        print '\n\033[0;37mWriting to \033[0;36m{0}:\
            \n\033[0;37m{1}\033[0m'.format(self.ndb_file, full_def)
        with open(self.ndb_file, 'a') as file_handle:
            file_handle.write(full_def)

def normalize_data(definition):
    '''Produces a clamav style normalized version of the text'''
    definition = re.sub('\n', ' ', definition)
    definition = re.sub(r'\s{2,}', ' ', definition)
    definition = re.sub(r'>\s+<', '><', definition)
    definition = re.sub(r'>\s+', '>', definition)
    definition = re.sub(r'\t', '', definition)
    definition = re.sub(r'=\s+', '=', definition)
    html_parser = HTMLParser()
    definition = html_parser.unescape(definition).lower()
    return definition

def get_input():
    '''Gets the input like a cat prompt'''
    banner = '''\033[1;35m         __      __    ___
  __ _  / /_____/ /__ / _/
 /  ' \/  '_/ _  / -_) _/
/_/_/_/_/\_\\_,_/\__/_/ \033[0m'''
    print banner
    print "\033[0;37mPaste the text to make a definition for\
        \n\033[1;32mctrl + D to confirm\n\033[0;31mctrl + C to quit\033[0m"
    contents = []
    while True:
        try:
            line = raw_input("")
        except EOFError:
            break
        contents.append(line)
    if len(contents) < 1:
        print 'Error: No input received'
        sys.exit(0)
    value = '\n'.join(contents).strip()
    if len(value) < 6:
        print 'Error: Definitions should be at least 6 characters.'
        sys.exit(0)
    return value

def main():
    '''function execution'''
    mkdef = MkDef()
    mkdef.check_ndb()
    def_name = mkdef.check_name()
    user_input = get_input()
    normalized = normalize_data(user_input)
    if user_input != normalized:
        mkdef.make_definition('{0}.Norm'.format(def_name), normalized.encode('hex'))
    mkdef.make_definition(def_name, user_input.encode('hex'))

if __name__ == '__main__':
    main()
